import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular'
import { FbInfosComponent } from './fb-infos/fb-infos';
@NgModule({
	declarations: [FbInfosComponent],
	imports: [IonicModule],
	exports: [FbInfosComponent]
})
export class ComponentsModule { }
