import { Component } from '@angular/core';
import { FbServiceProvider } from '../../providers/fb-service/fb-service';

/**
 * Generated class for the FbInfosComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'fb-infos',
  templateUrl: 'fb-infos.html'
})
export class FbInfosComponent {
  userProfile: any;
  text: string;

  constructor( public facebook: FbServiceProvider) {
    console.log('Hello FbInfosComponent Component');
    this.text = 'Hello World comme ça \n ttu comprends \n Oui';
  }


  getProfile() {
    this.facebook.getProfile().subscribe((profile)=>{   //Si connecté, on récupére le profil
      this.userProfile = JSON.stringify(profile);
      //this.navCtrl.setRoot(HomePage);
      alert(JSON.stringify(this.userProfile));
    }, (error)=>{console.log(error);});
  }

}
