import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { FbServiceProvider } from '../../providers/fb-service/fb-service';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  userProfile: any;
  
  constructor(public navCtrl: NavController,
    public facebook: FbServiceProvider) {

  }


  FBlogin() {
    this.facebook.login().subscribe((connected)=>{   //On surveille l'observable afin de récupérer le status de connection
      if(connected === true){
        this.facebook.getProfile().subscribe((profile)=>{   //Si connecté, on récupére le profil
          this.userProfile = JSON.stringify(profile);

          //this.navCtrl.setRoot(HomePage);
          alert(JSON.stringify(this.userProfile));
        }, (error)=>{console.log(error);});
      }
    }, (error)=>{console.log(error);});
  }



}
